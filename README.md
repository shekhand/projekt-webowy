# Projekt webowy

Projekt na przedmiot "Projekt zespołowy".

Link do działającej strony: http://shekhand.pythonanywhere.com/login/

Web aplikacja, back-end jest napisany w Python z użyciem frameworku Django, front-end HTML + CSS + Bootstrap.

Odpowiedzialny za back-end:
* [Andrii Shekhovtsov](https://gitlab.com/shekhand)

Odpowiedzialni za front-end:
* [Bartosz Kowalik](https://gitlab.com/patisonov)
* [Piotr](https://gitlab.com/piter4d)
* [Richard](https://gitlab.com/MeTheCoder)

Dokumentacja i info o projekcie jest na [wiki](https://gitlab.com/shekhand/projekt-webowy/-/wikis/home)
