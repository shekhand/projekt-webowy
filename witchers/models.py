from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.urls import reverse

from mysite.settings import MEDIA_URL


class UserExtension(models.Model):
    USER_TYPE = [ ('customer', 'Klient'), ('witcher', 'Wiedźmin') ]

    user            = models.OneToOneField(User, on_delete=models.CASCADE, unique=True, related_name='extension')
    fullname        = models.CharField(max_length=150, blank=True)
    avatar          = models.ImageField(upload_to='user_avatars/', blank=True, null=True)
    type            = models.CharField(max_length=10, choices=USER_TYPE, default='customer')

    def __str__(self):
        return str(self.user)

    def get_name(self):
        if self.fullname:
            return self.fullname
        else:
            return self.user.username

    def get_avatar(self):
        if self.avatar:
            return f'{MEDIA_URL}{self.avatar}'
        else:
            return f'{MEDIA_URL}user_avatars/no_image.png'

    def get_notification_count(self):
        '''Get number of this user's post where are unread comments'''
        user_ads = Ad.objects.filter(user=self.user)
        res = 0
        for ad in user_ads:
            res += 1 if ad.get_unread_comments_count() else 0

        all_posts_with_application = Ad.objects.filter(user=self.user)\
                .filter(category__name='Zlecenie')\
                .exclude(application__isnull=True)

        posts_with_application = all_posts_with_application\
                .filter(application__is_completed=False)

        unrated_posts = all_posts_with_application\
                .filter(application__is_completed=True)\
                .filter(application__rate=0)

        res += len(posts_with_application)
        res += len(unrated_posts)

        return res

    def get_url(self):
        return reverse('user_details', args=[self.user.username])


class Category(models.Model):
    name = models.CharField(max_length=32, unique=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Ad(models.Model):
    STATUSES = [ (0, 'Aktualne'), (1, 'Nie aktualne') ]

    slug        = models.SlugField(max_length=50, unique=True)
    status      = models.BooleanField(choices=STATUSES, default=0)
    title       = models.CharField(max_length=64)
    description = models.TextField()
    category    = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='+')
    price       = models.FloatField()
    image       = models.ImageField(upload_to='post_images/', blank=True, null=True)
    user        = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    created     = models.DateTimeField(auto_now=True)

    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Ad.objects.filter(slug=unique_slug).exists():
            unique_slug = f'{slug[:45]}-{num}'
            num += 1
        return unique_slug

    def get_created_time(self):
        now = timezone.now()
        if now.date() == self.created.date():
            return f'dzisiaj o {self.created.strftime("%H:%M")}'
        else:
            return f'{self.created.date()} o {self.created.strftime("%H:%M")}'

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('post_details', args=[self.slug])

    def get_url(self):
        return reverse('post_details', args=[self.slug])

    def get_delete_url(self):
        return reverse('post_details', args=[self.slug]) + 'delete'

    def get_edit_url(self):
        return reverse('post_details', args=[self.slug]) + 'edit'

    def get_change_status_url(self):
        return self.get_absolute_url() + 'change_status'

    def __str__(self):
        return self.title

    def get_image(self):
        if self.image:
            return f'{MEDIA_URL}{self.image}'
        else:
            return f'{MEDIA_URL}post_images/no_image.png'

    def mark_comments_as_read(self):
        unread_comments = self.comments.filter(is_read=False)
        for comment in unread_comments:
            comment.is_read = True
            comment.save()

    def get_unread_comments_count(self):
        return self.comments.filter(is_read=False).count()


class Application(models.Model):
    user         = models.ForeignKey(User, on_delete=models.CASCADE, related_name='applications')
    ad           = models.OneToOneField(Ad, on_delete=models.CASCADE, unique=True, related_name='application', null=True, blank=True)
    rate         = models.IntegerField(default=0)
    is_completed = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user}\'s application under {self.ad}'


class Comment(models.Model):
    user    = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    ad      = models.ForeignKey(Ad, on_delete=models.CASCADE, related_name='comments')
    text    = models.TextField(blank=False)
    created = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)

    def get_created_time(self):
        now = timezone.now()
        if now.date() == self.created.date():
            return f'dzisiaj o {self.created.strftime("%H:%M")}'
        else:
            return f'{self.created.date()} o {self.created.strftime("%H:%M")}'

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return 'Comment by {} on {}'.format(self.user, self.ad)
