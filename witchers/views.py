import django.http as http
from django.shortcuts import render, get_object_or_404
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.models import Group, User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Count
from django.utils import timezone

from .forms import LoginForm, RegistrationForm, AccountEditForm, UserEditForm, MyPasswordChangeForm, AddAdForm, CommentForm
from .models import UserExtension, Ad, Category, Application


def landing(request):
    if request.user.is_authenticated:
        return http.HttpResponseRedirect('main_page')

    if request.method == 'POST':
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        msg = request.POST.get('message', None)
        print(f'Message from {name} ({email}): {msg}.')

    last_posts = Ad.objects.filter(status=0).order_by('-created')[:6]
    return render(request, 'landing.html', {'posts': last_posts})


def user_login(request):
    next = request.GET.get('next', '')
    error = None
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request,
                                username=cd['username'],
                                password=cd['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                print("here")
                next_url = form.data['next']
                if not next_url:
                   next_url = '/main_page' #Change in future
                return http.HttpResponseRedirect(next_url)
            else:
                error = 'Konto nieaktywne.'
        else:
            error = 'Podałeś błędne dane.'
    else:
        form = LoginForm()
    return render(request, 'registration/login.html', {'form': form,
                                                       'next': next,
                                                       'error': error})


def user_registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.set_password(form.cleaned_data['password'])

            # Save user to db
            new_user.save()

            # Create new user extension object
            new_user_extension = UserExtension.objects.create(user=new_user)

            # Adding user to group
            user_type = form.cleaned_data['user_type']
            new_user_extension.type = user_type
            new_user_extension.save()

            return render(request,
                          'registration/register_done.html',
                          {'new_user': new_user,
                           'user_type': user_type })
    else:
        form = RegistrationForm()
    return render(request, 'registration/register.html', {'form': form})


def _get_posts(request,
               author=None,
               commented_by=None,
               category=None,
               title=None,
               price_bottom=None,
               price_top=None,
               order_by=None,
               active_only=False):
    if author:
        all_posts = Ad.objects.filter(user=author)
    elif commented_by:
        all_posts = Ad.objects.filter(comments__user=commented_by)
        all_posts = all_posts.annotate(dcount=Count('title'))
    else:
        all_posts = Ad.objects.all()

    if active_only:
        all_posts = all_posts.filter(status=0)

    if category:
        all_posts = all_posts.filter(category__name__exact=category)

    if title:
        all_posts = all_posts.filter(title__contains=title)

    if price_bottom:
        all_posts = all_posts.filter(price__gt=price_bottom)

    if price_top:
        all_posts = all_posts.filter(price__lt=price_top)

    if order_by:
        all_posts = all_posts.order_by(order_by)
    else:
        all_posts = all_posts.order_by('-created')

    paginator = Paginator(all_posts, 10)
    page = request.GET.get('page')

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    return posts, page


@login_required(login_url='/login')
def main_page(request):
    posts, page = _get_posts(request,
                             category=request.GET.get('category', None),
                             title=request.GET.get('title', None),
                             price_bottom=request.GET.get('price_bottom', None),
                             price_top=request.GET.get('price_top', None),
                             order_by=request.GET.get('sort', None),
                             active_only=True)

    categories = Category.objects.all()
    return render(request, 'witchers/main_page.html', {'posts': posts,
                                                       'categories': categories,
                                                       'args': request.GET})


@login_required(login_url='/login')
def my_ads(request):
    posts, page = _get_posts(request,
                             author=request.user,
                             category=request.GET.get('category', None),
                             title=request.GET.get('title', None),
                             price_bottom=request.GET.get('price_bottom', None),
                             price_top=request.GET.get('price_top', None),
                             order_by=request.GET.get('sort', None))

    categories = Category.objects.all()
    return render(request, 'witchers/myAdv.html', {'posts': posts,
                                                   'categories': categories,
                                                   'args': request.GET})


@login_required(login_url='/login')
def commented_ads(request):
    posts, page = _get_posts(request,
                             commented_by=request.user,
                             category=request.GET.get('category', None),
                             title=request.GET.get('title', None),
                             price_bottom=request.GET.get('price_bottom', None),
                             price_top=request.GET.get('price_top', None),
                             order_by=request.GET.get('sort', None))

    categories = Category.objects.all()
    return render(request,
                  'witchers/commented_ads.html',
                  {'posts': posts,
                   'categories': categories,
                   'args': request.GET})


@login_required(login_url='/login')
def unread_coments(request):
    posts = Ad.objects.filter(user=request.user).filter(comments__is_read=False)
    posts = posts.annotate(dcount=Count('id'))

    all_posts_with_application = Ad.objects.filter(user=request.user)\
            .filter(category__name='Zlecenie')\
            .exclude(application__isnull=True)

    posts_with_application = all_posts_with_application\
            .filter(application__is_completed=False)

    unrated_posts = all_posts_with_application\
            .filter(application__is_completed=True)\
            .filter(application__rate=0)

    return render(request,
                  'witchers/unread_comments.html',
                  {'posts': posts,
                   'posts_with_application': posts_with_application,
                   'unrated_posts': unrated_posts,
                   'disable_search': True})


@login_required(login_url='/login')
def post_details(request, post):
    try:
        post = Ad.objects.get(slug=post)
    except ObjectDoesNotExist:
        raise http.Http404()

    comment_added = False
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)

        try:
            last_user_comment = post.comments.filter(user=request.user).order_by('-created')[0]
            time_from_last_comment = (timezone.now() - last_user_comment.created).seconds
        except IndexError:
            time_from_last_comment = 61

        if comment_form.is_valid() and time_from_last_comment > 60:
            if time_from_last_comment > 60:
                new_comment = comment_form.save(commit=False)
                new_comment.user = request.user
                new_comment.ad = post
                new_comment.save()
                comment_added = True
            # Create new empty form
            comment_form = CommentForm()
        else:
            comment_form.add_error('text', 'Komentarze można dodawać raz na minute.')
    else:
        comment_form = CommentForm()

    if post.user == request.user:
        post.mark_comments_as_read()

        post.mark_comments_as_read()

    all_comments = post.comments.all()
    paginator = Paginator(all_comments, 10)
    page = request.GET.get('page')

    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)

    return render(request,'witchers/details.html',
                  {'post': post,
                   'comments': comments,
                   'comment_form': comment_form,
                   'disable_search': True,
                   'comment_added': comment_added})


@login_required(login_url='/login')
def change_post_status(request, post):
    try:
        post = Ad.objects.get(slug=post)
    except ObjectDoesNotExist:
        raise http.Http404()

    if post.category.name == 'Zlecenie' or request.user != post.user:
        raise PermissionDenied()
    else:
        post.status = int(not post.status)
        post.save()

    return http.HttpResponseRedirect(post.get_absolute_url())


@login_required(login_url='/login')
def delete_post(request, post):
    try:
        post = Ad.objects.get(slug=post)
    except ObjectDoesNotExist:
        raise http.Http404()

    if post.user == request.user:
        # Check if this ad has application object
        try:
            if post.application:
                return http.HttpResponseRedirect(f'/error/?prev=/my_ads/')
        except ObjectDoesNotExist:
            pass

        post.delete()
    else:
        raise PermissionDenied()
    return http.HttpResponseRedirect('/my_ads')


@login_required(login_url='/login')
def add_ad(request):
    if request.method == 'POST':
        form = AddAdForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            new_ad = form.save(commit=False)
            new_ad.user = request.user
            new_ad.save()
            return http.HttpResponseRedirect(f'/ad/{new_ad.slug}/')
    else:
        form = AddAdForm()
    return render(request, 'witchers/advForm.html', {'form': form,
                                                     'disable_search': True})


@login_required(login_url='/login')
def edit_post(request, post):
    try:
        post = Ad.objects.get(slug=post)
        if post.user != request.user:
            raise PermissionDenied()
    except ObjectDoesNotExist:
        raise http.Http404()

    try:
        if post.application:
            return http.HttpResponseRedirect(f'/error/?prev=/my_ads/')
    except ObjectDoesNotExist:
        pass

    if request.method == 'POST':
        form = AddAdForm(request.POST, files=request.FILES)
        if form.is_valid():
            cd = form.cleaned_data

            # Copy data from form to existed post
            post.title = cd['title']
            post.category = cd['category']
            post.price = cd['price']
            post.description = cd['description']
            post.image = cd['image']

            post.save()
            return http.HttpResponseRedirect(f'/ad/{post.slug}/')
    else:
        form = AddAdForm(instance=post)
    return render(request, 'witchers/editAdvForm.html', {'form': form,
                                                         'disable_search': True})


@login_required(login_url='/login')
def account_edit(request):
    # Get user extension of logged in user
    username = request.user.username
    user_ext = UserExtension.objects.get(user=request.user)

    user_changed = False
    password_changed = False

    if request.method == 'POST':
        if request.POST['form_name'] == 'account_edit':
            form = AccountEditForm(instance=user_ext,
                                   data=request.POST,
                                   files=request.FILES)
            user_form = UserEditForm(instance=request.user,
                                     data=request.POST)
            if form.is_valid() and user_form.is_valid():
                form.save()
                user_form.save()
                user_changed = True

            form_password = MyPasswordChangeForm(request.user)

        elif request.POST['form_name'] == 'password_change':
            form_password = MyPasswordChangeForm(user=request.user,
                                                 data=request.POST)
            if form_password.is_valid():
                form_password.save()
                update_session_auth_hash(request, form_password.user)
                password_changed = True

            user_form = UserEditForm(instance=request.user)
            form = AccountEditForm(instance=user_ext)

        else:
            return http.HttpResponse('Something goes wrong')

    else: # Create empty forms if it's GET request
        user_form = UserEditForm(instance=request.user)
        form = AccountEditForm(instance=user_ext)
        form_password = MyPasswordChangeForm(request.user)
    return render(request,
                  'registration/account_edit.html',
                  {'form': form,
                   'user_form': user_form,
                   'form_password': form_password,
                   'password_changed': password_changed,
                   'user_changed': user_changed,
                   'username': username})


@login_required(login_url='/login')
def account_delete(request):
    return render(request, 'registration/account_delete.html', { 'user': request.user })


@login_required(login_url='/login')
def account_delete_done(request):
    User.objects.get(username=request.user.username).delete()
    User.objects.filter(pk=request.user.pk).update(is_active=False, email=None)
    return render(request, "registration/account_delete_done.html")


def _get_post(request, post):
    try:
        post = Ad.objects.get(slug=post)
    except ObjectDoesNotExist:
        raise http.Http404()
    return post


@login_required(login_url='/login')
def apply_to_post(request, post):
    post = _get_post(request, post)
    if request.user.extension.type != 'witcher'\
            or post.category.name != 'Zlecenie':
        raise PermissionDenied()

    try:
        appl = post.application
    except ObjectDoesNotExist:
        Application.objects.create(ad=post, user=request.user)
        return http.HttpResponseRedirect(post.get_absolute_url())

    return http.HttpResponse('Nie możesz zgłosić się kilka razy.')


def _get_post_and_application(request, post):
    post = _get_post(request, post)
    try:
        appl = post.application
    except ObjectDoesNotExist:
        appl = None

    if not appl\
            or appl.user != request.user:
        raise PermissionDenied()
    return post, appl


@login_required(login_url='/login')
def cancel_post_application(request, post):
    post, appl = _get_post_and_application(request, post)

    appl.ad = None
    appl.rate = 1
    appl.is_completed = True
    appl.save()

    return http.HttpResponseRedirect(post.get_absolute_url())


@login_required(login_url='/login')
def mark_post_as_completed(request, post):
    post, appl = _get_post_and_application(request, post)

    appl.is_completed = True
    appl.save()

    return http.HttpResponseRedirect(post.get_absolute_url())


@login_required(login_url='/login')
def rate_witcher_work(request, post):
    post = _get_post(request, post)
    try:
        appl = post.application
    except ObjectDoesNotExist:
        appl = None

    if not appl\
            or post.user != request.user\
            or not appl.is_completed:
        raise PermissionDenied()

    rate = request.GET.get('rate', None)
    if rate and not appl.rate:
        appl.rate = rate
        appl.save()
        post.status = 1
        post.save()

    return http.HttpResponseRedirect(post.get_absolute_url())


def _get_user_stats(user):
    if user.extension.type == 'customer':
        stats = None
    else:
        applications = user.applications.exclude(is_completed=False)
        if len(applications) != 0:
            completed = applications.exclude(rate=0).exclude(ad__isnull=True)
            canceled = applications.filter(ad=None)
            rate = sum([appl.rate for appl in applications]) / (len(completed) + len(canceled))

            stats = {'completed': len(completed),
                     'all': len(applications),
                     'canceled': len(canceled),
                     'rate': rate}
        else:
            stats = None
    return stats


@login_required(login_url='/login')
def user_details(request, username):
    try:
        user = User.objects.get(username=username)
    except ObjectDoesNotExist:
        return http.HttpResponse('Użytkownik nie istnieje')

    if user == request.user:
        return http.HttpResponseRedirect('/my_ads')

    stats = _get_user_stats(user)

    posts, page = _get_posts(request,
                             author=user,
                             category=request.GET.get('category', None),
                             title=request.GET.get('title', None),
                             price_bottom=request.GET.get('price_bottom', None),
                             price_top=request.GET.get('price_top', None),
                             order_by=request.GET.get('sort', None),
                             active_only=True)

    return render(request, 'witchers/user_adv.html', {'user':user,
                                                      'posts':posts,
                                                      'stats':stats})


@login_required(login_url='/login')
def witchers_stats(request):
    if request.user.extension.type != 'witcher':
        raise PermissionDenied()

    stats = _get_user_stats(request.user)

    all_applications = request.user.applications.exclude(ad__isnull=True)

    uncompleted_applications = all_applications\
            .filter(is_completed=False)

    completed_applications = all_applications\
            .filter(is_completed=True)\
            .exclude(rate=0)

    completed_unrated_applications = all_applications\
            .filter(is_completed=True)\
            .filter(rate=0)

    return render(request, 'witchers/witchers_stats.html',
                  {'uncompleted': uncompleted_applications,
                   'completed': completed_applications,
                   'unrated': completed_unrated_applications,
                   'stats':stats})


@login_required(login_url='/login')
def error(request):
    return render(request,
                  'witchers/error.html',
                  {'prev': request.GET.get('prev', None)})
