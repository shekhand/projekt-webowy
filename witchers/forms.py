from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordChangeForm
import django.core.files.images as django_images

from .models import UserExtension

from .models import Ad, Category, Comment


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control",
        "id": "nameControlInput",
        "placeholder": "Jak się nazywasz?",
        "maxlength": "20",}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        "class": "form-control",
        "id": "passwordControlInput",
        "placeholder": "Sekretne hasło..."
        }))


class RegistrationForm(forms.ModelForm):
    error_css_class = 'error-text'
    user_type = forms.ChoiceField(
        widget=forms.RadioSelect(attrs={"class": "form-check-input"}),
        choices=(('customer', "Klient"), ('witcher', "Wiedźmin")),
        initial='customer'
        )
    username = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control",
        "id": "nameControlInput",
        "placeholder": "Jak się nazywasz?",
        "data-incorrect": "false",
        "maxlength": "20",}),
        error_messages={ 'unique': "Użytkownik z takim loginem już istnieje." }
        )
    email = forms.CharField(widget=forms.EmailInput(attrs={
        "class": "form-control",
        "id": "emailControlInput",
        "placeholder": "Wpisz adres e-mail...",
        "data-incorrect": "false",
        "maxlength": "150",}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        "class": "form-control",
        "id": "passwordControlInput",
        "placeholder": "Sekretne hasło...",
        "data-incorrect": "false",
        "maxlength": "150",}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={
        "class": "form-control",
        "id": "repeatPasswordControlInput",
        "placeholder": "Powtórz hasło...",
        "data-incorrect": "false",
        "maxlength": "150",}))

    class Meta:
        model = User
        fields = ('username', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd["password"] != cd["password2"]:
            raise forms.ValidationError("Passwords don't match.")
        return cd["password2"]


class AddAdForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'id': 'advTitle',
        'required': True,
        'placeholder': 'Wpisz tu nazwę zlecenia',
        }))

    description = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control col-12',
        'id': 'advDescription',
        'required': True,
        'rows': '3',
        'placeholder': 'Opisz tu szczegóły zlecenia',
        }))

    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        empty_label='Wybierz kategorie',
        widget=forms.Select(attrs={
            'class': 'form-control col-12',
            'id': 'advType',
            'required': True,
            }))

    price = forms.FloatField(
        min_value=0,
        max_value=20000,
        error_messages={'min_value': 'Wartość ma być większa od zera!',
                        'max_value': 'Wartość ma być mniejsza lub równa 20000!'},
        widget=forms.NumberInput(attrs={
            'class': 'form-control col-12',
            'id': 'advCost',
            'placeholder': 'Cena',
            'required': True,
            }))

    image = forms.ImageField(widget=forms.FileInput(), required=False)

    class Meta:
        model = Ad
        fields = ('title', 'description', 'category', 'price', 'image')


class AccountEditForm(forms.ModelForm):
    fullname = forms.CharField(widget=forms.TextInput(attrs={
        "class": "form-control",
        "id": "fullnameControlInput",
        "placeholder": "Jak się nazywasz?"
    }), required=False)
    avatar = forms.ImageField(widget=forms.FileInput(), required=False)
    class Meta:
        model = UserExtension
        fields = ('fullname', 'avatar',)

    def clean_avatar(self):
        avatar = self.cleaned_data.get('avatar', False)
        if avatar:
            w, h = django_images.get_image_dimensions(avatar)
            if avatar.size > 2*1024*1024 and w != 200 and h != 200:
                raise forms.ValidationError('To musi być obrazek do 2MB i rozmiarem 200px na 200px')
            return avatar


class UserEditForm(forms.ModelForm):
    email = forms.CharField(widget=forms.EmailInput(attrs={
        "class": "form-control",
        "id": "emailControlInput",
        "placeholder": "Wpisz nowy adres e-mail...",
        "data-incorrect": "false",
    }))
    class Meta:
        model = User
        fields = ('email',)


class MyPasswordChangeForm(PasswordChangeForm):
    error_messages = {
        'password_incorrect': 'Stare hasło jest nieprawiedłowe.',
        'password_mismatch': 'Hasła nie zgadzają się.',
    }

    old_password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'current-password',
            'placeholder': 'Wpisz swoje stare hasło'
    }))
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'current-password',
            'placeholder': 'Wpisz nowe hasło'
        }),
        strip=False,
    )
    new_password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={
            'autocomplete': 'current-password',
            'placeholder': 'Powtórz nowe hasło'
    }))


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)
