from django.contrib import admin
from .models import *

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
    list_display = ('title', 'category',)
    search_fielts = ('title',)
    ordering = ('title',)


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'ad', 'created',)
    search_field = ('user', 'ad',)


@admin.register(Application)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('user', 'ad',)
    search_field = ('user',)


@admin.register(UserExtension)
class UserExtensionAdmin(admin.ModelAdmin):
    list_display = ('user',)
