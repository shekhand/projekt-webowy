from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.landing, name='landing'),

    path('login/', views.user_login, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('registration/', views.user_registration, name='registration'),
    path('account_edit/', views.account_edit, name='account_edit'),
    path('account_delete/', views.account_delete, name='account_delete'),
    path('account_delete_done/', views.account_delete_done, name='account_delete_done'),

    path('main_page/', views.main_page, name='main_page'),

    path('user/<username>/', views.user_details, name='user_details'),
    path('witchers_stats/', views.witchers_stats, name='witchers_stats'),

    path('add_ad/', views.add_ad, name='add_ad'),
    path('ad/<slug:post>/', views.post_details, name='post_details'),
    path('ad/<slug:post>/change_status', views.change_post_status, name='change_status'),
    path('ad/<slug:post>/delete', views.delete_post, name='delete_post'),
    path('ad/<slug:post>/edit', views.edit_post, name='edit_post'),

    path('ad/<slug:post>/apply', views.apply_to_post, name='apply'),
    path('ad/<slug:post>/cancel_application', views.cancel_post_application, name='cancel_application'),
    path('ad/<slug:post>/mark_completed', views.mark_post_as_completed, name='mark_completed'),
    path('ad/<slug:post>/rate', views.rate_witcher_work, name='rate'),

    path('my_ads/', views.my_ads, name='my_ads'),
    path('commented_ads/', views.commented_ads, name='commented_ads'),
    path('unread_comments/', views.unread_coments, name='unread_comments'),

    path('error/', views.error, name='error'),


    # Password reset
    path('password_reset/',
          auth_views.PasswordResetView.as_view(),
          name='password_reset'),
    path('password_reset/done/',
          auth_views.PasswordResetDoneView.as_view(),
          name='password_reset_done'),
    path('reset/<uidb64>/<token>/',
          auth_views.PasswordResetConfirmView.as_view(),
          name='password_reset_confirm'),
    path('reset/done/',
          auth_views.PasswordResetCompleteView.as_view(),
          name='password_reset_complete')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
