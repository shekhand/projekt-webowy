'use strict';

describe("validatePassword", function() {
    it("Haslo 'test' jest niepoprawne", function() {
        assert.equal(validatePassword("test"), false);
    });

    it("Haslo 'abc' jest niepoprawne", function() {
        assert.equal(validatePassword("abc"), false);
    });

    it("Haslo 'Abc123' jest niepoprawne", function() {
        assert.equal(validatePassword("Abc123"), false);
    });

    it("Haslo ' ' jest niepoprawne", function() {
        assert.equal(validatePassword(" "), false);
    });

    it("Haslo '==' jest niepoprawne", function() {
        assert.equal(validatePassword("=="), false);
    });

    it("Haslo ' ab=c123    ' jest niepoprawne", function() {
        assert.equal(validatePassword(" ab=c123    "), false);
    });

    it("Haslo 'Nietakszybko123' jest poprawne", function() {
        assert.equal(validatePassword("Nietakszybko123"), true);
    });
});

describe("validateName", function() {
    it("Imię '  Adrian' jest poprawne", function() {
        assert.equal(validateName('  Adrian'), true);
    });

    it("Imię 'Geralt ' jest poprawne", function() {
        assert.equal(validateName('Geralt'), true);
    });

    it("Imię 'h3h3' jest poprawne", function() {
        assert.equal(validateName('h3h3'), true);
    });

    it("Imię 'rupert3210  ' jest poprawne", function() {
        assert.equal(validateName('rupert3210  '), true);
    });

    it("Imię '12345' jest niepoprawne", function() {
        assert.equal(validateName('12345'), false);
    });

    it("Imię 'Karol Kowalski' jest niepoprawne", function() {
        assert.equal(validateName('Karol Kowalski'), false);
    });

    it("Imię 'Huna-una' jest poprawne", function() {
        assert.equal(validateName('Huna-una'), true);
    });

    it("Imię 'to_ja_Karol' jest poprawne", function() {
        assert.equal(validateName('to_ja_Karol'), true);
    });
});