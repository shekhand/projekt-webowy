$(document).ready(function(){
	
	$('.carousel').carousel();
	
	let counter;
	
	if(($( document ).width()) <= 992)
		counter = 4;
	else
		counter = 6;
	
	$('body').on('click', 'a[href^="#"]', function (event) {
		event.preventDefault();

		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 500);
	});
	
	if(($( document ).width()) <= 992) 
	{
		$("#lp-description").css("padding","2rem");
		$("#lp-description h1").removeClass("text-left").css("margin","0 auto");
		$(".title").css("font-size","2rem");
		$(".logo p").css("font-size","200%");
		$("#lp-example .login-info-wrapper p").css("font-size","2rem");
		$("#lp-contact h1").css("margin-bottom","2rem");
	}
	else 
	{
		$("#lp-description").css("padding","3rem");
		$("#lp-description h1").addClass("text-left").css("margin","0");
		$(".title").css("font-size","2.5rem");
		$(".logo p").css("font-size","400%");
		$("#lp-example .login-info-wrapper p").css("font-size","3rem");
	}
})
