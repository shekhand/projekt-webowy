'use strict';

const textInput = document.getElementsByTagName("textarea")[0];
const btSubmit = document.getElementById("bt-comment");

btSubmit.onclick = function() {
    if(textInput.length !== 0)
        document.forms[0].submit();
};