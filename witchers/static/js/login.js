'use strict';

const errorMsgIdxs = {
    login: 0,
    password: 1,
};

const btNoActionAnim = {
    keyframes: [ {transform: 'translateX(0)'},
        {transform: 'translateX(4%)'},
        {transform: 'translateX(-4%)'},
        {transform: 'translateX(0)'} ],
    options: {
        duration: 500,
        iterations: 1
    }
};

const incorrectColor = '#e74c3c';
const correctColor = '#ffffff';

const loginInput = document.getElementById('nameControlInput');
const passwordInput = document.getElementById('passwordControlInput');
const errorMessages2 = document.getElementsByClassName('error-text');
const btLogin = document.getElementById('bt-login');

const markField2 = (field, errorMsgIndex, isRed) => {
    if(isRed) {
        field.style.backgroundColor = incorrectColor;
        errorMessages2[errorMsgIndex].setAttribute("style",
            `color: ${incorrectColor}; font-weight: 600; display: block;`);
    }
    else {
        field.style.backgroundColor = correctColor;
        errorMessages2[errorMsgIndex].setAttribute('style',
            'display: none');
    }
};

loginInput.addEventListener('change', function() {
    if(!validateName(loginInput.value)) {
        loginInput.dataset.incorrect = "true";
        markField2(loginInput, errorMsgIdxs["login"], true);
    }
    else {
        markField2(loginInput, errorMsgIdxs["login"], false);
        loginInput.dataset.incorrect = "false";
    }
});

passwordInput.addEventListener('change', function() {
    if(!validatePassword(passwordInput.value)) {
        passwordInput.dataset.incorrect = "true";
        markField2(passwordInput, errorMsgIdxs["password"], true);
    }
    else {
        markField2(passwordInput, errorMsgIdxs["password"], false);
        passwordInput.dataset.incorrect = "false";
    }
});

function onSubmitValidation() {
    const incorrectIndexes = [];
    const inputs = [loginInput, passwordInput];
    let index = 0;

    for(const input of inputs) {
        const valueLength = input.value.trim().length;

        if(input.dataset.incorrect === "true" || valueLength === 0) {
            incorrectIndexes.push(index);

            if(valueLength === 0)
                markField2(input, index++, true);
        }
    }

    if(incorrectIndexes.length === 0)
        document.forms[0].submit();
    else
        btLogin.animate(btNoActionAnim.keyframes, btNoActionAnim.options);
};

btLogin.onclick = onSubmitValidation;

document.addEventListener('keydown', function(event) {
    if(event.code === 'Enter') {
        onSubmitValidation();
    }
});

function validatePassword(password) {
    const regex = new RegExp('^[a-zA-Z1-9]{8,}$');
    return regex.test(password.trim());
}

function validateName(name) {
    const regex = new RegExp('^[a-zA-Z][-A-Za-z0-9_]{1,}$');
    return regex.test(name.trim());
}