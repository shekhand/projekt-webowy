'use strict';

const buttonNoActionAnim = {
    keyframes: [ {transform: 'translateX(0)'},
        {transform: 'translateX(4%)'},
        {transform: 'translateX(-4%)'},
        {transform: 'translateX(0)'} ],
    options: {
        duration: 500,
        iterations: 1
    }
};

const incorrectColor2 = '#e74c3c';
const correctColor2 = '#ffffff';

const errorMsgIndexes = {
    email: 0,
    fullName: 1,
    oldPasswd: 2,
    newPasswd: 3,
    rePasswd: 4
};

const emailInput = document.getElementById('emailControlInput');
const fullNameInput = document.getElementById('fullnameControlInput');
const oldPasswdInput = document.getElementById('id_old_password');
const newPasswdInput = document.getElementById('id_new_password1');
const rePasswdInput = document.getElementById('id_new_password2');
const errorMessages = document.getElementsByClassName('error-text');

const markField = (field, errorMsgIndex, isRed) => {
    if(isRed) {
        field.style.backgroundColor = incorrectColor2;
        errorMessages[errorMsgIndex].setAttribute("style",
            `color: ${incorrectColor2}; font-weight: 600; display: block;`);
    }
    else {
        field.style.backgroundColor = correctColor2;
        errorMessages[errorMsgIndex].setAttribute('style',
            'display: none');
    }
};

emailInput.addEventListener('change', function() {
    if(!validateEmail(emailInput.value)) {
        emailInput.dataset.incorrect = "true";
        markField(emailInput, errorMsgIndexes["email"], true);
    }
    else {
        markField(emailInput, errorMsgIndexes["email"], false);
        emailInput.dataset.incorrect = "false";
    }
});

fullNameInput.addEventListener('change', function() {
    if(!validateName(fullNameInput.value)) {
        fullNameInput.dataset.incorrect = "true";
        markField(fullNameInput, errorMsgIndexes["fullName"], true);
    }
    else {
        markField(fullNameInput, errorMsgIndexes["fullName"], false);
        fullNameInput.dataset.incorrect = "false";
    }
});

oldPasswdInput.addEventListener('change', function() {
    if(!validatePassword(oldPasswdInput.value)) {
        oldPasswdInput.dataset.incorrect = "true";
        markField(oldPasswdInput, errorMsgIndexes["oldPasswd"], true);
    }
    else {
        markField(oldPasswdInput, errorMsgIndexes["oldPasswd"], false);
        oldPasswdInput.dataset.incorrect = "false";
    }
});

newPasswdInput.addEventListener('change', function() {
    if(!validatePassword(newPasswdInput.value)) {
        newPasswdInput.dataset.incorrect = "true";
        markField(newPasswdInput, errorMsgIndexes["newPasswd"], true);
    }
    else {
        markField(newPasswdInput, errorMsgIndexes["newPasswd"], false);
        newPasswdInput.dataset.incorrect = "false";

        if(rePasswdInput.value === newPasswdInput.value) {
            markField(rePasswdInput, errorMsgIndexes["rePasswd"], false);
            rePasswdInput.dataset.incorrect = "false";
        }
    }
});

rePasswdInput.addEventListener('change', function() {
    if(rePasswdInput.value !== newPasswdInput.value) {
        rePasswdInput.dataset.incorrect = "true";
        markField(rePasswdInput, errorMsgIndexes["rePasswd"], true);
    }
    else {
        markField(rePasswdInput, errorMsgIndexes["rePasswd"], false);
        rePasswdInput.dataset.incorrect = "false";
    }
});

const buttonForm1 = document.getElementById('form1bt');
const buttonForm2 = document.getElementById('form2bt');

buttonForm1.onclick = function() {
    if(!validateEmail(emailInput.value)) {
        markField(emailInput, errorMsgIndexes["email"], true);
        buttonForm1.animate(buttonNoActionAnim.keyframes, buttonNoActionAnim.options);
    }
    else if(!validateName(fullNameInput.value)) {
        markField(fullNameInput, errorMsgIndexes["fullName"], true);
        buttonForm1.animate(buttonNoActionAnim.keyframes, buttonNoActionAnim.options);
    }
    else {
        document.forms[0].submit();
    }
};

buttonForm2.onclick = function () {
    if(oldPasswdInput.value.length === 0 || oldPasswdInput.dataset.incorrect === "true") {
        markField(oldPasswdInput, errorMsgIndexes['oldPasswd'], true);
        buttonForm2.animate(buttonNoActionAnim.keyframes, buttonNoActionAnim.options);
    }
    else if(newPasswdInput.value.length === 0 || newPasswdInput.dataset.incorrect === "true") {
        markField(newPasswdInput, errorMsgIndexes['newPasswd'], true);
        buttonForm2.animate(buttonNoActionAnim.keyframes, buttonNoActionAnim.options);
    }
    else if(rePasswdInput.value.length === 0 || rePasswdInput.dataset.incorrect === "true") {
        markField(rePasswdInput, errorMsgIndexes['rePasswd'], true);
        buttonForm2.animate(buttonNoActionAnim.keyframes, buttonNoActionAnim.options);
    }
    else {
        document.forms[1].submit();
    }
};

function validateName(name) {
    if(name.trim().length === 0)
        return true;
    else {
        const regex = new RegExp('^[a-zA-Z]{2,}\\s+[a-zA-Z]{2,}$');
        return regex.test(name.trim());
    }
}

function validatePassword(password) {
    const regex = new RegExp('^[a-zA-Z1-9]{8,}$');
    return regex.test(password.trim());
}

function validateEmail(email) {
    const regex = new RegExp('^((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])$');
    return regex.test(email.trim());
}