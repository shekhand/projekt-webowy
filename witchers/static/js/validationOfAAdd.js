
document.addEventListener("DOMContentLoaded", function () {
    const cfg = {
        colorGood: 'rgba(46, 46, 46,0)',
        colorBad: 'rgb(138, 49, 49)'
    };
    const form = new ValidateForm(document.querySelector('#advForm'), cfg);
});

class ValidateForm {

    constructor(form, options) {

        this.form = form;

        this.style = options;

        this.form.setAttribute('novalidate', 'novalidate');

        this.localStorageItems = {
            title: "advTitle",
            nameItem: "advNameItem",
            cost: "advCost",
            type: "advType",
            description: "advDescription",
            beastName: "advBeastName"
        }
        this.prepareForm();

        this.onSubmit();

    };

    prepareForm() {
        const elements = this.form.querySelectorAll('[required]');
        this.setValuesFromLocal();
        [].forEach.call(elements, function (element) {
            if (element.id == this.localStorageItems["title"]) {
                element.addEventListener('input', function () {
                    this.setLocalValue(this.localStorageItems["title"],element.value);
                    this.validateTitle(element);
                }.bind(this));
            } else if (element.id == this.localStorageItems["cost"]) {
                element.addEventListener('input', function () {
                    this.setLocalValue(this.localStorageItems["cost"],element.value);
                    this.validateNumber(element);
                }.bind(this));
            }else if (element.id == this.localStorageItems["type"]) {
                this.valueOfType = element.options[element.selectedIndex].value;
                element.addEventListener('change', function () {
                    this.setLocalValue(this.localStorageItems["type"],element.value);
                    this.validateType(element);
                }.bind(this));
            }
            else if (element.id == this.localStorageItems["description"]) {
                element.addEventListener('input', function () {
                    this.setLocalValue(this.localStorageItems["description"],element.value);
                    this.validateDesc(element);
                }.bind(this));
            } 
        }, this);
    };

    onSubmit() {
        this.form.addEventListener('submit', function (form) {
            form.preventDefault();

            const elements = this.form.querySelectorAll('[required]');
            let isGood = true;


            [].forEach.call(elements, function (element) {
                if (element.id == this.localStorageItems["title"]) {
                    if (!this.validateTitle(element)) {
                        isGood = false;
                    }
                } else if (element.id == this.localStorageItems["cost"]) {
                    if (!this.validateNumber(element)) {
                        isGood = false;
                    }
                }
                else if (element.id == this.localStorageItems["description"]) {
                    if (!this.validateDesc(element)) {
                        isGood = false;
                    }
                }else if (element.id == this.localStorageItems["type"]) {
                   if(!this.validateType(element)){
                       isGood=false;
                   }
                }
            }, this);

            if (isGood) {
                this.form.submit();
                this.clearLocalStorage();
            }

        }.bind(this));
    }

    validateTitle(input) {
        const reg = new RegExp('^[a-zA-Z0-9ąćęłńóśźżĄĘŁŃÓŚŹŻ .!?]{3,64}$');

        if (reg.test(input.value.trim())) {
            this.showFieldValidation(input, true);
            return true;
        } else {
            this.showFieldValidation(input, false);
            return false;
        }
    }

    validateNumber(input) {
        const reg = new RegExp('^[0-9]{1,10}(\.|,)?[0-9]{0,2}$');
        const numbOfInput = parseFloat(input.value);
        if (reg.test(input.value.trim()) &&  numbOfInput <= 20000){
            this.showFieldValidation(input, true);
            return true;
        } else {
            this.showFieldValidation(input, false);
            return false;
        }
    }

    validateDesc(input) {
        const reg = new RegExp('^[a-zA-Z0-9ąćęłńóśźżĄĘŁŃÓŚŹŻ -:,.?!\n()]{10,400}$');

        if (reg.test(input.value.trim())) {
            this.showFieldValidation(input, true);
            return true;
        } else {
            this.showFieldValidation(input, false);
            return false;
        }
    }

    validateType(input) {
        this.valueOfType = input.options[input.selectedIndex].value;
        console.log(this.valueOfType);
        if (this.valueOfType == null ||this.valueOfType == "" ) {
            this.showFieldValidation(input, false);
            return false;
        } else{
            this.showFieldValidation(input, true);
            return true;
        }
    }

    showFieldValidation(input, inputIsValid) {
        if (!inputIsValid) {
            input.style.setProperty('background-color',this.style.colorBad,'important');
            this.makeVisable(input.nextElementSibling);
        } else {
            input.style.setProperty('background-color',this.style.colorGood,'important');
            this.makeHidden(input.nextElementSibling);
        }
    };

    makeHidden(id) {
        if(id!=null)
            id.style.setProperty('display', 'none');
    }
    makeVisable(id) {
        if(id!=null)
            id.style.setProperty('display', 'block');
    }

    setValuesFromLocal(){
        for (var key in this.localStorageItems) {
            const valueStorage = localStorage.getItem(this.localStorageItems[key]);
            const element = document.querySelector("#"+this.localStorageItems[key]);
            if (valueStorage != null) {
                if(element!=null){
                    element.value = valueStorage;
                }
            }
        }
    }
    setLocalValue(name,value){
        const valueStorage = localStorage.getItem(name);
        if(valueStorage!=null){
            localStorage.removeItem(name);
            localStorage.setItem(name,value);
        }else{
            localStorage.setItem(name,value);
        }
    }

    clearLocalStorage(){
        for(var key in this.localStorageItems){
            localStorage.removeItem(this.localStorageItems[key]);
        }
    }
}
