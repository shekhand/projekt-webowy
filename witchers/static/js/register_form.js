'use strict';

const btNoActionAnim = {
    keyframes: [ {transform: 'translateX(0)'},
        {transform: 'translateX(4%)'},
        {transform: 'translateX(-4%)'},
        {transform: 'translateX(0)'} ],
    options: {
        duration: 500,
        iterations: 1
    }
};

const errorMsgIdxs = {
  login: 0,
  email: 1,
  password: 2,
  rePassword: 3
};

const loginInput = document.getElementById('nameControlInput');
const emailInput = document.getElementById('emailControlInput');
const passwordInput = document.getElementById('passwordControlInput');
const rePasswordInput = document.getElementById('repeatPasswordControlInput');
const errorMessages = document.getElementsByClassName('error-text');
const btSubmit = document.getElementById('bt-submit');
const incorrectColor = '#e74c3c';
const correctColor = '#ffffff';

const markField = (field, errorMsgIndex, isRed) => {
    if(isRed) {
        field.style.backgroundColor = incorrectColor;
        errorMessages[errorMsgIndex].setAttribute("style",
            `color: ${incorrectColor}; font-weight: 600; display: block;`);
    }
    else {
        field.style.backgroundColor = correctColor;
        errorMessages[errorMsgIndex].setAttribute('style',
            'display: none');
    }
};

loginInput.addEventListener('change', function() {
    if(!validateName(loginInput.value)) {
        loginInput.dataset.incorrect = "true";
        markField(loginInput, errorMsgIdxs["login"], true);
    }
    else {
        markField(loginInput, errorMsgIdxs["login"], false);
        loginInput.dataset.incorrect = "false";
    }
});

emailInput.addEventListener('change', function() {
    if(!validateEmail(emailInput.value)) {
        emailInput.dataset.incorrect = "true";
        markField(emailInput, errorMsgIdxs["email"], true);
    }
    else {
        markField(emailInput, errorMsgIdxs["email"], false);
        emailInput.dataset.incorrect = "false";
    }
});

passwordInput.addEventListener('change', function() {
    if(!validatePassword(passwordInput.value)) {
        passwordInput.dataset.incorrect = "true";
        markField(passwordInput, errorMsgIdxs["password"], true);
    }
    else {
        markField(passwordInput, errorMsgIdxs["password"], false);
        passwordInput.dataset.incorrect = "false";

        if(rePasswordInput.value === passwordInput.value) {
            markField(rePasswordInput, errorMsgIdxs["rePassword"], false);
            rePasswordInput.dataset.incorrect = "false";
        }
    }
});

rePasswordInput.addEventListener('change', function() {
    if(rePasswordInput.value !== passwordInput.value) {
        rePasswordInput.dataset.incorrect = "true";
        markField(rePasswordInput, errorMsgIdxs["rePassword"], true);
    }
    else {
        markField(rePasswordInput, errorMsgIdxs["rePassword"], false);
        rePasswordInput.dataset.incorrect = "false";
    }
});

function onSubmitValidation() {
    const incorrectIndexes = [];
    const inputs = [loginInput, emailInput, passwordInput, rePasswordInput];
    let index = 0;

    for(const input of inputs) {
        const valueLength = input.value.trim().length;

        if(input.dataset.incorrect === "true" || valueLength === 0) {
            incorrectIndexes.push(index);

            if(valueLength === 0)
                markField(input, index, true);

            index++;
        }
    }

    if(incorrectIndexes.length === 0)
        document.forms[0].submit();
    else
        btSubmit.animate(btNoActionAnim.keyframes, btNoActionAnim.options);
}

btSubmit.onclick = onSubmitValidation;

document.addEventListener('keydown', function(event) {
   if(event.code === 'Enter') {
       onSubmitValidation();
   }
});

function validatePassword(password) {
    const regex = new RegExp('^[a-zA-Z1-9]{8,}$');
    return regex.test(password.trim());
}

function validateName(name) {
    const regex = new RegExp('^[a-zA-Z][-A-Za-z0-9_]{1,}$');
    return regex.test(name.trim());
}

function validateEmail(email) {
    const regex = new RegExp('^((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])$');
    return regex.test(email.trim());
}