
document.addEventListener("DOMContentLoaded", function () {
    positionTopBody();
    window.addEventListener("resize", positionTopBody);
    const menu = document.getElementById("menuMob");
    const header = document.getElementsByClassName("abc");
    const buttonMenuMob = document.getElementById("menuButt");
    const xBtn = document.getElementById('closeBtn');

    buttonMenuMob.addEventListener("click", () => menu.classList.toggle("show"));
    xBtn.addEventListener("click", () => menu.classList.remove("show"));
});


function positionTopBody() {
    var headerHeight = document.querySelector('.panel-heading').offsetHeight;
    document.querySelector('.panel-body').style.setProperty('top',headerHeight + "px");
}
