from django.apps import AppConfig


class WitchersConfig(AppConfig):
    name = 'witchers'
